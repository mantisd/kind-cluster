## TO INSTALL
enable the proxy in your CLI

Tools Needed
- kubectl
- helm
- kind
- Terraform

Kind
: https://itada.opr.statefarm.org/itada-web/toolshed/Kind

Helm
: https://sf-charts.sfgitlab.opr.statefarm.org/helm-docs/installation/


## Helm Repos
| Chart | URL |
|---|---|
| Metallb | https://metallb.github.io/metallb |
| Consul | https://helm.releases.hashicorp.com |
| Traefik | https://helm.traefik.io/traefik |
| K8s Dashboard | https://kubernetes.github.io/dashboard |

Having Issues with the k8s dashboard service account not allowing you to view any resources.

> If you get a metallb helm error, 

> make sure proxy_on 

> helm repo add https://metallb.github.io/metallb

> helm repo update


# Terraform install
cd into the infra folder

## Setup Gitlab Remote State

create a new state.tfvars file
```
development_remote_state_address="https://sfgitlab.opr.statefarm.org/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${state_name}"
development_username= "${Your USERNAME}"
development_access_token="${Create a Gitlab Access Token}"
```

After that is complete...

```
terraform init

terraform plan

terraform apply -auto-approve
```

this will 
- helm install metallb
- create address pool
- helm install traefik
- add default header middleware
- add traefik dashboard ingressRoute

Dashboard 
: https://traefik.localhost 

Example app not deployed


### If having issues just delete backend and use local 


## MetalLB
### IF IP IS INCORRECT IN ADDRESSPOOL RANGE
Inspect the network to get the IP range
```
docker network inspect -f '{{.IPAM.Config}}' kind
```

In the example my output was 
172.22.0.0/16

In addressPool.yaml set the range to be inside this subnet


## Links
Dashboard 
: https://traefik.localhost 

Dashboard
: https://localhost
: WIP 

# Private Registry
To access your private registry we will create a secret name regcred that contains our docker config, so make sure you can login to the registry locally and pull containers

```bash
kubectl create secret generic regcred --from-file=.dockerconfigjson=/path/to/.docker/config.json --type=kubernetes.io/dockerconfigjson
```

now add this secret as an imagePullSecret
```yaml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: private-container-app
  namespace: default
  labels:
    app: private-container-app
spec:
  replicas: 3
  progressDeadlineSeconds: 600
  revisionHistoryLimit: 2
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: private-container-app
  template:
    metadata:
      labels:
        app: private-container-app
    spec:
      containers:
      - name: private-container-app
        image: registry.sfgitlab.opr.statefarm.org/sf-private-image
      imagePullSecrets:  #HERE
      - name: regcred 
```
Containers should pull without issues, check with
```
kubectl get pods -n <namespace> 
```

# Acceess Dashboard
---
```
kubectl describe secret admin-user-secret -n kubernetes-dashboard
```
copy token

go to https://localhost

enter token


# Resources
Kind
: https://kind.sigs.k8s.io/

MetalLB Kind Setup
: https://kind.sigs.k8s.io/docs/user/loadbalancer/

MetalLb
: https://metallb.universe.tf/

Traefik
: https://doc.traefik.io/traefik/
