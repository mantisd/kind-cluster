terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    kubectl = {
        source = "gavinbunney/kubectl"
    }
    kind = {
      source = "tehcyx/kind"
      version = "0.0.13"
    }
  }
}

resource "kubernetes_namespace" "dashboard" {
    metadata {
        name = "kubernetes-dashboard"
    }
}

resource "helm_release" "dashboard" {
    name = "dashboard"
    repository = "https://kubernetes.github.io/dashboard/"
    chart = "kubernetes-dashboard"
    namespace = kubernetes_namespace.dashboard.metadata.0.name

    depends_on = [
       kubernetes_namespace.dashboard
    ]
}

resource "kubectl_manifest" "k8s_clusterrole" {
    yaml_body = <<YAML
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
rules:
  # Allow Metrics Scraper to get metrics from the Metrics server
  - apiGroups: ["metrics.k8s.io"]
    resources: ["pods", "nodes"]
    verbs: ["get", "list", "watch"]
YAML

depends_on = [ helm_release.dashboard ]
}

resource "kubectl_manifest" "k8s_role" {
    yaml_body = <<YAML
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
rules:
  # Allow Dashboard to get, update and delete Dashboard exclusive secrets.
  - apiGroups: ["*"]
    resources: ["*"]
    resourceNames: ["*"]
    verbs: ["get", "update", "delete", "list"]
YAML

depends_on = [ helm_release.dashboard ]
}

resource "kubectl_manifest" "k8s_serviceaccount" {
    yaml_body = <<YAML
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: kubernetes-dashboard
subjects:
  - kind: ServiceAccount
    name: kubernetes-dashboard
    namespace: kubernetes-dashboard

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
YAML

depends_on = [ helm_release.dashboard ]
}

resource "kubectl_manifest" "k8s_dashoard_token" {
    yaml_body = <<YAML
apiVersion: v1
kind: Secret
metadata:
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
  annotations:
    kubernetes.io/service-account.name: kubernetes-dashboard
type: kubernetes.io/service-account-token
YAML

depends_on = [ kubectl_manifest.k8s_serviceaccount ]
}


resource "kubectl_manifest" "k8s_dashoard" {
    yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: k8s-dashboard
  namespace: kubernetes-dashboard
  annotations: 
    kubernetes.io/ingress.class: traefik-external
spec:
  entryPoints:
    - websecure
  routes:
    - match: Host(`k8s.localhost`)
      kind: Rule
      services:
        - name: dashboard-kubernetes-dashboard
          port: 443
      middlewares:
        - name: default-headers
          namespace: default
  tls: {}
YAML

depends_on = [ helm_release.dashboard ]
}
