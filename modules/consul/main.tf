terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    kubectl = {
        source = "gavinbunney/kubectl"
    }
    kind = {
      source = "tehcyx/kind"
      version = "0.0.13"
    }
  }
}

resource "kubernetes_namespace" "consul" {
    metadata {
        name = "consul"
    }
}

resource "helm_release" "consul" {
    name = "consul"
    repository = "https://helm.releases.hashicorp.com"
    chart = "consul"
    version = "0.43.0"
    namespace = kubernetes_namespace.consul.metadata.0.name

    values = [
        templatefile("${path.module}/values.tmpl", {
        })
    ]

    depends_on = [
       kubernetes_namespace.consul
    ]
}
