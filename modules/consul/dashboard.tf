resource "kubectl_manifest" "dashoard" {
yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: consul-dashboard
  namespace: consul
  annotations: 
    kubernetes.io/ingress.class: traefik-external
spec:
  entryPoints:
    - websecure
  routes:
    - match: Host(`consul.k8s.localhost`)
      kind: Rule
      services:
        - name: consul-ui
          port: 80
      middlewares:
        - name: default-headers
          namespace: default
  tls: {}
YAML

depends_on = [ helm_release.consul ]
}
