terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    kubectl = {
        source = "gavinbunney/kubectl"
    }
    kind = {
      source = "tehcyx/kind"
      version = "0.0.13"
    }
  }
}

resource "kubernetes_namespace" "argocd" {
    metadata {
        name = "argocd"
    }
}

resource "helm_release" "argocd" {
    name = "argocd"
    repository = "https://argoproj.github.io/argo-helm"
    chart = "argo-cd"
    version = "5.10.0"
    namespace = kubernetes_namespace.argocd.metadata.0.name

    values = [
        templatefile("${path.module}/values.tmpl", {
        })
    ]

    depends_on = [
       kubernetes_namespace.argocd
    ]
}
