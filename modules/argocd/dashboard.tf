resource "kubectl_manifest" "dashoard" {
yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: argocd-ingress-route
  namespace: argocd
  annotations: 
    kubernetes.io/ingress.class: traefik-external
spec:
  entryPoints:
    - websecure
    - web
  routes:
    - kind: Rule
      match: Host(`argocd.localhost`)
      services:
        - name: argocd-server
          port: 443
          scheme: https

  tls: {}
YAML

depends_on = [ helm_release.argocd ]
}
