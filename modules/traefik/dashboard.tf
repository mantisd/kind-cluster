resource "kubectl_manifest" "dashoard" {
    yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: traefik-dashboard
  namespace: traefik
  annotations: 
    kubernetes.io/ingress.class: traefik-external
spec:
  entryPoints:
    - websecure
  routes:
    - match: Host(`traefik.localhost`)
      kind: Rule
      # middlewares:
       # - name: traefik-dashboard-basicauth
       #  namespace: traefik
      services:
        - name: api@internal
          kind: TraefikService
#  tls:
#    secretName: local-example-com-staging-tls
YAML

depends_on = [ helm_release.traefik ]
}

resource "kubectl_manifest" "default_headers" {
    yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: Middleware
metadata:
  name: default-headers
  namespace: default
spec:
  headers:
    browserXssFilter: true
    contentTypeNosniff: true
    forceSTSHeader: true
    stsIncludeSubdomains: true
    stsPreload: true
    stsSeconds: 15552000
    customFrameOptionsValue: SAMEORIGIN
    customRequestHeaders:
      X-Forwarded-Proto: https
YAML

depends_on = [ helm_release.traefik ]
}
