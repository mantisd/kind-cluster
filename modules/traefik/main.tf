terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    kubectl = {
        source = "gavinbunney/kubectl"
    }
    kind = {
      source = "tehcyx/kind"
      version = "0.0.13"
    }
  }
}

resource "kubernetes_namespace" "traefik" {
    metadata {
        name = "traefik"
    }
}

resource "helm_release" "traefik" {
    name = "traefik"
    repository = "https://helm.traefik.io/traefik"
    chart = "traefik"
    namespace = kubernetes_namespace.traefik.metadata.0.name

    values = [
        templatefile("${path.module}/values.tmpl", {
            allowCrossNamespace = true
            replicas = 3
            load_balancer_IP = var.load_balancer_IP
        })
    ]

    depends_on = [
       kubernetes_namespace.traefik
    ]
}