terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    kubectl = {
        source = "gavinbunney/kubectl"
    }
    kind = {
      source = "tehcyx/kind"
      version = "0.0.13"
    }
  }
}

resource "kubernetes_namespace" "metallb_system" {
    metadata {
        name = "metallb-system"
    }
}

resource "helm_release" "metallb_system" {
    name = "metallb"
    repository = "https://metallb.github.io/metallb"
    chart = "metallb"
    namespace = kubernetes_namespace.metallb_system.metadata.0.name

    values = [
        templatefile("${path.module}/values.tmpl", {})
    ]

    depends_on = [
       kubernetes_namespace.metallb_system
    ]
}