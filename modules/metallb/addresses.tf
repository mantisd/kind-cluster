resource "kubectl_manifest" "address_pool" {
    yaml_body = <<YAML
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: ips-pool
  namespace: metallb-system
spec:
  addresses:
  - 172.22.1.10-172.22.1.100
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: ips-advert
  namespace: metallb-system
YAML

depends_on = [ helm_release.metallb_system ]
}