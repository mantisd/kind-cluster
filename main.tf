terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0"
    }
    kubectl = {
        source = "gavinbunney/kubectl"
    }
    kind = {
      source = "tehcyx/kind"
      version = "0.0.13"
    }
  }
}


locals {
    k8s_config_path = pathexpand("~/.kube/config")
}

provider "kind" {}

provider "kubernetes" {
  host                   = kind_cluster.default.endpoint
  client_certificate     = kind_cluster.default.client_certificate
  client_key             = kind_cluster.default.client_key
  cluster_ca_certificate = kind_cluster.default.cluster_ca_certificate
}
provider "kubectl" {
  host                   = kind_cluster.default.endpoint
  client_certificate     = kind_cluster.default.client_certificate
  client_key             = kind_cluster.default.client_key
  cluster_ca_certificate = kind_cluster.default.cluster_ca_certificate
}
provider "helm" {
  kubernetes {
  host                   = kind_cluster.default.endpoint
  client_certificate     = kind_cluster.default.client_certificate
  client_key             = kind_cluster.default.client_key
  cluster_ca_certificate = kind_cluster.default.cluster_ca_certificate
  }
}


#this allows the cluster to fully deploy before we add resources
resource "time_sleep" "wait" {
  depends_on = [kind_cluster.default]
  create_duration = "20s"
}

module "metallb" {
    source = "./modules/metallb/"
    providers = {
      kubernetes = kubernetes 
      helm = helm
      kubectl = kubectl
   }
   depends_on = [
     time_sleep.wait
   ]
}


module "traefik" {
    source = "./modules/traefik/"
    load_balancer_IP = "172.22.1.10"
    providers = {
      kubernetes = kubernetes 
      helm = helm
      kubectl = kubectl
    }

    depends_on = [
      module.metallb
    ]
}

module "argocd" {
    source = "./modules/argocd/"
    providers = {
      kubernetes = kubernetes 
      helm = helm
      kubectl = kubectl
    }

    depends_on = [
      module.traefik
    ]
}


/* 
module "consul" {
    source = "./modules/consul/"
    providers = {
      kubernetes = kubernetes 
      helm = helm
      kubectl = kubectl
    }

    depends_on = [
      module.traefik
    ]
} */
