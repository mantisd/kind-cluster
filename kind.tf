resource "kind_cluster" "default" {
    name = "kind-cluster"
    kubeconfig_path = local.k8s_config_path
    kind_config {
      kind = "Cluster"
      api_version = "kind.x-k8s.io/v1alpha4"
      node {
          role = "worker"
      }
      node {
          role = "worker"
      }
      node {
        role = "control-plane"
        extra_port_mappings  {
            container_port = 30001
            host_port      = 80
        }
        extra_port_mappings  {
            container_port = 30000
            host_port      = 443
        }
      }
      networking {
        pod_subnet = "172.22.0.0/16"
        api_server_port = 6443
        api_server_address = "127.0.0.1"
      }
    }
} 